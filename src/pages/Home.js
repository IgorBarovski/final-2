import React, { useEffect, useState } from 'react'
import HeaderSlider from '../components/HeaderSlider/HeaderSlider'
import Products from '../components/Products/Products'
import { useLoaderData } from 'react-router-dom'

const Home = () => {
 const [products,setProducts] = useState([])
 const data = useLoaderData()
 useEffect(()=>{
  setProducts(data.data)
 },[data])

  return (

  <>  
  
  <HeaderSlider/>
  <Products products={products}/>
</>

     
  
  )
}

export default Home
