
import React, { useEffect, useState } from "react";
import CartItem from '../components/CartItem/CartItem'  
import './Cart.css'
import { useSelector} from 'react-redux'
import { Link } from "react-router-dom"
import CreditCart from "../components/CreditCart/CreditCart";


const Cart = () => {
  const productData = useSelector((state)=>state.booriva.productData)


  const [totalAmt, setTotalAmt] = useState("");
  useEffect(() => {
    let price = 0;
    productData.map((item) => {
      price += item.price * item.quantity;
      return price;
    });
    setTotalAmt(price.toFixed(2));
  } );





  return (
    <>
 
    <div className='basket__block '>
        <div className='basket__block__wrapper '>

              <div >
              <CartItem/>
              </div>
          

                  
        </div>

        <div className=' total__block '>


                    <div className='total__block__info'>

                    <h2>Totals</h2>
                    <p>Subtotal
                      <span>&#8364;{totalAmt}</span>
                    </p>
            
                    </div>
                    
                  <Link to='/order'>
                    <button  className='total__btn'>TO PAY</button>
                    </Link>
                    <p className="total__block__info__conf">By clicking, you agree to the processing of data on the issuance of data in accordance with the rules of application in the framework of fixing video recording operations of the issuance of orders in the framework of the exclusion of the issuance of orders and the execution of trading operations.</p>
                  </div>
                
    </div>
    </>
  )
}

export default Cart
