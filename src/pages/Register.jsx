import React from 'react'
import './Register.css'
import { Button, message } from 'antd';

import {  Link } from "react-router-dom"
const Register = () => {

    const [messageApi, contextHolder] = message.useMessage();
  const key = 'updatable';


  const openMessage = () => {
    messageApi.open({
      key,
      type: 'loading',
      content: 'Registrati...',
    });
    setTimeout(() => {
      messageApi.open({
        key,
        type: 'success',
        content: 'Registration successfully completed',
        duration: 2,
      });
    }, 2000);
  };
  return (
    <>

    <form />
  <div className="container">
    <h2>Вход</h2>
    <p>Пожалуйста, заполните эту форму, чтобы создать учетную запись.</p>
    <hr/>

    <label for="email"><b>Email</b></label>
    <input className='input' type="text" placeholder=" Ведите Email" name="email" required/>

    <label for="psw"><b>Пароль</b></label>
    <input className='input' type="password" placeholder=" Ведите пароль" name="psw" required/>

    <label for="psw-repeat"><b>Повторите пароль</b></label>
    <input className='input'type="password" placeholder="Повторите пароль" name="psw-repeat" required/>
    
    <label>
      <input className='input' type="checkbox"  name="remember" /> Запомнить меня
    </label>
    
    <p>Создавая учетную запись, вы соглашаетесь с нашим <a href="#" >Условием и конфиденциальностью</a>.</p>

    <div className="clearfix">
    {contextHolder}
      <Button type="primary" onClick={openMessage} className='Btn__submit' >
       
Register
      </Button>
      <Button><Link to="/">Close</Link></Button>
    </div>
    
  </div>
  </>
  )
}

export default Register
