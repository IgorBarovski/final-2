import { createSlice } from "@reduxjs/toolkit";


const initialState ={
    productData:[],
    userInfo:null,
   
}
export const boorivaSlice=createSlice({
    name:"booriva",
    initialState,
    reducers:{
        addToCart:(state,action)=>{
            const item=state.productData.find((item)=>item._id===action.payload._id)

            if(item){
                item.quantity+=action.payload.quantity
            }else{
                state.productData.push(action.payload)
            }
            
        },
        addQuantity: (state, action) => {
            const item = state.productData.find(
              (item) => item._id === action.payload._id
            );
            if (item) {
              item.quantity++;
            }
          },
          disAddQuantity: (state, action) => {
            const item = state.productData.find(
              (item) => item._id === action.payload._id
            );
            if (item.quantity === 1) {
              item.quantity = 1;
            } else {
              item.quantity--;
            }
          },
          deleteItem: (state, action) => {
            state.productData = state.productData.filter(
              (item) => item._id !== action.payload
            );
          },
          resetCart: (state) => {
            state.productData = [];
          },
    },

})
export const {
    addToCart,
    deleteItem,
    resetCart,
    addQuantity,
    disAddQuantity
}=boorivaSlice.actions
export default boorivaSlice.reducer