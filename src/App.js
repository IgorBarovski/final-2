import React from "react";
import Home from "./pages/Home";
import Header from "./components/Header/Header";
import Footer from "./components/Footer/Footer";
import Cart from "./pages/Cart"
import Order from "./pages/Order"
import {
  createBrowserRouter,
  Outlet,
  RouterProvider,
} from "react-router-dom";
import { productsData } from "./api/Api";
import Product from "./components/Product/Product";
import Shop from "./pages/Shop";
import Register from "./pages/Register";

const Layout = ()=>{
  return(
    <div className="wrapper"> 
    <Header/>


    <Outlet/>

   

    <Footer/>
  </div>
  )
  
}

const router =createBrowserRouter([

  {
    path:"/",
    element:<Layout/>,
    children:[
      {
        path:"/",
        element:<Home/>,
        loader:productsData
      },
      {
        path:"/product/:id",
      element:<Product/>,

      },
      {
        path:"cart",
        element:<Cart/>,
      },
      {
        path:"order",
        element:<Order/>,
      },
      {
        path:"shop",
        element:<Shop/>,
      },
      {
        path:"register",
        element:<Register/>,
      },
    ],
  },
])



function App() {
  return (
    <div className="App">
      

    
            <RouterProvider router={router}/>
           
    
    </div>
  );
}

export default App;
