import React, { useState } from 'react';
import { HeadLogo,Vectorbasket } from '../../arsenal'
import { useParams, Link } from "react-router-dom"
import './Header.css'
import { useSelector } from 'react-redux'

import { UserOutlined     } from '@ant-design/icons';
import Modal from 'react-modal';

import {
  Button,
  Cascader,
  DatePicker,
  Form,
  Input,
  InputNumber,
  Radio,
  Select,
  Switch,
  TreeSelect,
  Checkbox,
} from 'antd';



const Header = () => {
  const productData =useSelector((state)=>state.booriva.productData)
  console.log(productData);


  const [modalIsOpen, setModalIsOpen] = useState(false);

  const openModal = () => {
    setModalIsOpen(true);
    return(
      <>wdwdwdw</>
    )
  };
  
  const closeModal = () => {
    setModalIsOpen(false);
  };
  const onFinish = (values) => {
    console.log('Success:', values);
  };
  const onFinishFailed = (errorInfo) => {
    console.log('Failed:', errorInfo);
  };
  



  const modalContent = (
    <div className='formInput'>
      <Form
    name="basic"
    labelCol={{
      span: 8,
    }}
    wrapperCol={{
      span: 16,
    }}
    style={{
      maxWidth: 600,
    }}
    initialValues={{
      remember: true,
    }}
    onFinish={onFinish}
    onFinishFailed={onFinishFailed}
    autoComplete="off"
  >
    <Form.Item className='s'
      label="Username"
      name="username"
      rules={[
        {
          required: true,
          message: 'Please input your username!',
        },
      ]}
    >
      <Input />
    </Form.Item>

    <Form.Item
      label="Password"
      name="password"
      rules={[
        {
          required: true,
          message: 'Please input your password!',
        },
      ]}
    >
      <Input.Password />
    </Form.Item>

    <Form.Item
      name="remember"
      valuePropName="checked"
      wrapperCol={{
        offset: 8,
        span: 16,
      }}
    >
      <Checkbox>Remember me</Checkbox>
    </Form.Item>

    <Form.Item
      wrapperCol={{
        offset: 8,
        span: 16,
      }}
    >
      <Button type="primary" htmlType="submit"  className='Btn__submit'><Link to="/"> Submit</Link>
       
      </Button>
      <Button onClick={closeModal}>Сlose</Button>
      <Link to='./Register' onClick={closeModal}>Register</Link>
    </Form.Item>
  </Form>
      
    </div>
  );

  
  
  return (
    <div className=' wrapper__header '>

      <div className=' header__top '>

      <Link to="/"> <div >

<img className= 'head__logo ' src={HeadLogo} alt='HeadLogo'/>
</div></Link>
      
        
           

            <div className='nav '>
          
                <ul className='nav__items '>
              
                
                  <Link to='./shop'>  
                  <li>Reviews</li>
                  </Link>
                
                
                
                </ul>
              <Link to='./cart'>
              <div className='nav__items__basket '>
                <img className='nav__items__basket__image '  src={Vectorbasket} alt='ImageBasket'/>

                <span className='nav__items__basket__count '>{productData.length}</span>
                
              </div>
              </Link>
              <div>
              <div >
                <Link  onClick={openModal} ><UserOutlined  className='userout'/></Link>
              
                </div>
    <Modal isOpen={modalIsOpen} onRequestClose={closeModal}>
      {modalContent}
    </Modal>
  </div>
            
  
              
            
            </div>
          
      </div>
      
    </div>
  )
}

export default Header
