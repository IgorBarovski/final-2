import React, { Component } from 'react'
import './Modal.css'

import { Button } from 'antd';



class Modal extends Component {

  state = {
    comments: [],
    form: {
      name: '',
      comment: ''
    }
  }

  componentDidMount() {
    if (localStorage.getItem('state')) {
      this.setState({ ...JSON.parse(localStorage.getItem('state')) })
    }
  }

  addComment = () => {
    this.setState({
      comments: [
        ...this.state.comments,
        {
          
          name: this.state.form.name,
          comment: this.state.form.comment,
          date: new Date()
        }
      ],
      form: {
        name: '',
        comment: ''
      }
    }, () => localStorage.setItem('state', JSON.stringify(this.state)))
  }

  removeComment = (id) => {
    this.setState({
      comments: this.state.comments.filter(comment => comment.id !== id)
    }, () => localStorage.setItem('state', JSON.stringify(this.state)))
  }

  handleChange = (e) => {
    console.log(e.target.name)
    this.setState({
      form: {
        ...this.state.form,
        [e.target.name]: e.target.value,
      }
    })
  }

  render() {
    return (
      <div className="comment__block">
        {this.state.comments.map(comment => <div className="comment__block__info"key={comment.id}>
     
          <strong>{comment.name} </strong>
          <span>{comment.comment}</span>
         
          <Button onClick={this.removeComment.bind(null, comment.id)}>Delete</Button>
        </div>)}
    
        <div className='comment__block__item'>
          <label>Your Name: <input
            type="text"
            value={this.state.form.name}
            name="name"
            onChange={this.handleChange} /></label>
          <label>Your comments: <textarea
            name="comment"
            value={this.state.form.comment}
            onChange={this.handleChange}></textarea>
          </label>
          <Button onClick={this.addComment}>Add</Button>
        </div>
      </div>
    )
  }
}

export default Modal