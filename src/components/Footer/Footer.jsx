import React from 'react'
import './footer.css'
import { HeadLogo } from '../../arsenal'
import { AppleOutlined,GithubOutlined ,YoutubeOutlined,LinkedinOutlined,EnvironmentOutlined ,PhoneOutlined,MailOutlined,UserOutlined,MonitorOutlined,CarOutlined ,MessageOutlined      } from '@ant-design/icons';


const Footer = () => {
  return (
    <div className='footer '>

        <div className='footer__wrapper '>
                <div>
                <img className='logo' src={HeadLogo} alt='HeadLogo'/>

                <div className='footer__icons'>
                     <AppleOutlined  className='icons__items'/>
                     <GithubOutlined className='icons__items'/>
                     <YoutubeOutlined className='icons__items'/> 
                     <LinkedinOutlined className='icons__items'/>

                </div>
                </div>

         
               
                 <div className='footer__contact'>
                    <h2 >Location Us</h2>

                    <div className='footer__contact'>
                            <div className='footer__contact__item '><EnvironmentOutlined /><a href="https://goo.gl/maps/e1kugyZ2ERB9tvWT9" target='_blank'><p>1268 King St E, Hamilton, ON L8N 1G8</p></a></div>
                          <div className='footer__contact__item '><PhoneOutlined /> <a href="tel:+8 0 25 43 43 ">+8 0 25 43 43</a></div> 
                          <div className='footer__contact__item '><MailOutlined /><a href="mailto:borriva@gmail.com target='_blank' ">borriva@gmail.com</a></div>    
                    </div>
                      
                 </div>

                 <div className=' footer__profile '>
                    <h2 >Profile</h2>

                    <div className='footer__contact '>
                            <p className='footer__contact__item '>
                              <span><UserOutlined /></span>
                              my account</p>
                    </div>

                    <div className='footer__contact '>
                            <p className='footer__contact__item '>
                              <span><MonitorOutlined /></span>
                              chekout</p>
                    </div>
                    <div className='footer__contact '>
                            <p className='footer__contact__item '>
                              <span><CarOutlined /></span>
                              order tracking</p>
                    </div>
                    <div className='footer__contact '>
                            <p className='footer__contact__item '>
                              <span><MessageOutlined /></span>
                              hepl & support</p>
                    </div>
                      
                 </div>
                  <div className='footer__subscride  '>
                    <input type="email" placeholder='Email' />
                   <button>Subscride</button>
                  </div>


        </div>
   
    </div>
  )
}

export default Footer
