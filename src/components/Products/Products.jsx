import React from 'react'
import ProductsCart from '../ProductCart/ProductsCart'
import './products.css'



const Products = ({products}) => {

  return (
    <> 
    <div className='header__products'> 
      <h1>Shop Now</h1>

<p>In this store you will find one-stop solutions</p>  
 </div>

    


    <div className='products__cart ' >
     {
      products.map((item)=>(
          <ProductsCart key={item._id} product={item}/>
      ))}
     
    </div>
    
    
    
    </>
   

  
  )
}

export default Products
