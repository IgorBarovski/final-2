import React, { useEffect, useState } from 'react'
import './CartItem.css'
import { useSelector} from 'react-redux'
import {CloseSquareOutlined   } from '@ant-design/icons';
import { useDispatch } from 'react-redux'
import {
  addQuantity,
  deleteItem,
  disAddQuantity,
  resetCart,
} from "../../redux/boorivaSlice";
import { Image, message, Space,Button } from 'antd';
import {  Link } from "react-router-dom"




const CartItem = () => {
  const productData = useSelector((state)=>state.booriva.productData)
console.log(productData);
  const dispatch = useDispatch()
  const [messageApi, contextHolder] = message.useMessage();

  const error = () => {
    messageApi.open({
      type: 'error',
      content: ` Item removed from cart`,
    });
  };
  const warning = () => {
    messageApi.open({
      type: 'warning',
      content: 'Cart is empty!',
    });
  };
  return (
    <>
    <div className='cart__item'>
      <div className='shooping__cart__block'>
          <h2>Shopping cart</h2>
      </div>
      <div>



        {productData.map((item)=>(
            <div className='cart__item__block ' key={item._id}>

                <div className='cart__item__block__info'>

                {contextHolder}
      <Space>
           <button onClick={error} className='cart__item__block__btn'>
           <CloseSquareOutlined className='cart__item__close__btn ' onClick={() =>
                      dispatch(deleteItem(item._id))
                    } />  
           </button>
      </Space>
           
             <img className='cart__item__img ' src={item.image} alt='productImage'></img>
          
               
               
                </div>
               
                  <p>{item.title}</p>
               
                  <div className=''>
                  <p className=''>Quantity</p>
                  <div className='qaunts__block'>
                    <span
                      onClick={() =>
                        dispatch(
                          disAddQuantity({
                            _id: item._id,
                            title: item.title,
                            image: item.image,
                            price: item.price,
                            quantity: 1,
                            description: item.description,
                          })
                        )
                      }
                      className='qaunts'
                    >
                      -
                    </span>
                  
                    {item.quantity}

                    <span 
                      onClick={() =>
                        dispatch(
                          addQuantity({
                            _id: item._id,
                            title: item.title,
                            image: item.image,
                            price: item.price,
                            quantity: 1,
                            description: item.description,
                          })
                        )
                      }
                      className='qaunts'
                    >
                      +
                    </span>


                    <p>{item.price*item.quantity}&#8364;</p>
                   
                  </div>
           
           

                </div>

        

            </div>






        ))}
        
      </div>
      
      {contextHolder}
      <Space>
      <button className='basket__btn reset__btn ' onClick={() =>
              dispatch(resetCart()) 
            }  > 
        <p onClick={warning}>Reset Cart </p>
        </button>
             </Space>


<Link to="/">
        <button className="basket__btn   gohome__btn">
          
          go shopping
        </button>
              </Link>


            

    </div>
    </>
  )
}

export default CartItem
